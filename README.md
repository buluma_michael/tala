Ansible Role: mysql_query
===================

Back up mysql tables into S3

Install
-------

Clone Repo and navigate to examples

### Requirements:

* python bindings for mysql ([python-mysqldb](https://packages.debian.org/jessie/python-mysqldb) on Debian, MySQL-python on RedHat/Fedora), just like the core mysql_* modules.
* Boto

You can install the dependencies as follows.

    $ yum install mysql-devel
    $ yum install MySQL-python
	$ pip install boto

### Running the examples from sources

Make sure you have a running mysql server.

Run the following commands insides examples/

	$ ansible-playbook dump.yml